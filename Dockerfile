FROM java:8

MAINTAINER yhou
ENV PARAMS="--spring.profiles.active=test"

WORKDIR /data/app
ADD  target/mini-server-0.0.1-SNAPSHOT.jar /app.jar

ENTRYPOINT ["sh","-c","java -jar -XX:+UseG1GC  -Dlogging.path=/log  /app.jar --spring.profiles.active=test"]
